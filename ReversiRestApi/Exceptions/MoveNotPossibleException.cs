﻿using ReversiRestApi.Models;
using System;

namespace ReversiRestApi.Exceptions
{
    public class MoveNotPossibleException : Exception
    {
        public MoveNotPossibleException() : base()
        { }

        public MoveNotPossibleException(string msg) : base(msg)
        { }
        
        public MoveNotPossibleException(Vector2 position) : base($"Zet ({position.Y},{position.X}) is niet mogelijk!")
        { }
    }
}

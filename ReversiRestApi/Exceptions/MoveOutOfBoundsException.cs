﻿using ReversiRestApi.Models;
using System;

namespace ReversiRestApi.Exceptions
{
    public class MoveOutOfBoundsException : MoveNotPossibleException
    {
        public MoveOutOfBoundsException() : base()
        {
        }

        public MoveOutOfBoundsException(Vector2 position) : base($"Zet ({position.Y},{position.X}) ligt buiten het bord!")
        { }
    }
}

﻿using System;

namespace ReversiRestApi.Models
{
    public class Vector2 : ICloneable
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Vector2(int x, int y)
        {
            X = x;
            Y = y;
        }

        public object Clone()
        {
            return new Vector2(X, Y);
        }
    }

}

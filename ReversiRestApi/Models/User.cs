﻿using System.Collections.Generic;

namespace ReversiRestApi.Models
{
    public class User
    {
        public string Id { get; set; }
        public IList<Roles> Roles { get; set; }
        //public string Token { get; set; }

        public User(string id, IList<Roles> roles)
        {
            Id = id;
            Roles = roles;
        }

        //public User(string id, Roles role, string token) : this(id, role)
        //{
        //    Token = token;
        //}
    }

    public enum Roles
    {
        Player,
        Mediator,
        Admin
    }
}

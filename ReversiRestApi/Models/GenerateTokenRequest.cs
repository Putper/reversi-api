﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReversiRestApi.Models
{
    public class GenerateTokenRequest
    {
        public string Id { get; set; }
        public IList<string> Roles { get; set; }

        public IList<Roles> GetRoles()
        {
            IList<Roles> roles = new List<Roles>();
            foreach(string role in Roles)
            {
                Enum.TryParse(role, out Roles typedRole);
                roles.Add(typedRole);
            }
            return roles;
        }

        public User ToUser()
        {
            return new User(Id, GetRoles());
        }
    }
}

﻿using ReversiRestApi.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReversiRestApi.Models
{
    public class Game : IGame
    {
        public string Token { get; set; }
        public string Description { get; set; }
        public string Player1Token { get; set; }
        public string Player2Token { get; set; }
        public Colour[,] Board { get; set; }
        public Colour Turn { get; set; }
        public bool IsFinished { get; set; }
        public Colour Winner { get; set; } = Colour.None;

        public delegate bool DirectionDelegate(Vector2 position);
        private DirectionDelegate[] _directionDelegates = new DirectionDelegate[8];

        public Game()
        {
            IsFinished = false;
            // generate random token
            Token = Convert.ToBase64String(Guid.NewGuid().ToByteArray());
            Token = Token.Replace("/", "q");
            Token = Token.Replace("+", "r");
            // assign random player to start

            Random random = new Random();
            Turn = random.NextDouble() >= 0.5 ? Colour.Black : Colour.White;

            ResetBoard();

            _directionDelegates[0] = DirectionTop;
            _directionDelegates[1] = DirectionTopRight;
            _directionDelegates[2] = DirectionRight;
            _directionDelegates[3] = DirectionBottomRight;
            _directionDelegates[4] = DirectionBottom;
            _directionDelegates[5] = DirectionBottomLeft;
            _directionDelegates[6] = DirectionLeft;
            _directionDelegates[7] = DirectionTopLeft;
        }

        /// <summary>
        /// set default board state. All colours set to empty, except for the centre 4 ones.
        /// </summary>
        private void ResetBoard()
        {
            Board = new Colour[8, 8]
            {
                { Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None },
                { Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None },
                { Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None },
                { Colour.None, Colour.None, Colour.None, Colour.White, Colour.Black, Colour.None, Colour.None, Colour.None },
                { Colour.None, Colour.None, Colour.None, Colour.Black, Colour.White, Colour.None, Colour.None, Colour.None },
                { Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None },
                { Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None },
                { Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None, Colour.None },
            };
        }


        /// <summary>
        /// Check if game is ended
        /// </summary>
        /// <returns>true of game has ended, else false</returns>
        public bool AnyMovesPossible()
        {
            // check for every position on the board
            // if a move is still possible
            for (int row = 0; row < Board.GetLength(0); ++row)
            {
                for (int column = 0; column < Board.GetLength(1); ++column)
                {
                    // if there's still a move possible
                    // the game hasn't ended yet
                    if (MovePossible(row, column))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Attempt to perform a move for the current Turn colour
        /// </summary>
        /// <param name="rowMove">Y position to place</param>
        /// <param name="columnMove">X position to place</param>
        /// <returns>the amount of discs that were laid down. 0 means it was an invalid move and no move was performed</returns>
        public int DoMove(int rowMove, int columnMove)
        {
            Vector2 position = new Vector2(columnMove, rowMove);
            if (rowMove < 0 || rowMove > 7 || columnMove < 0 || columnMove > 7)
                throw new MoveOutOfBoundsException(position);

            // fire a move to each direction
            int laidDiscsCount = 0;
            for (int i = 0; i < _directionDelegates.Length; ++i)
            {
                DirectionDelegate directionDelegate = _directionDelegates[i];
                int currentDiscsLaid = DoMove(position, directionDelegate);
                // if 0 were conquered, its an invalid move
                if (currentDiscsLaid > 0)
                    laidDiscsCount += currentDiscsLaid;
            }

            // ensure one move was succesful, else it's an invalid move
            if (laidDiscsCount <= 0)
                throw new MoveNotPossibleException(position);

            // switch turn
            Turn = OppositeColour(Turn);

            // if new player can't do any moves, switch turn again
            if (!AnyMovesPossible())
            {
                // if neither players can do a move, game is finished!
                Turn = OppositeColour(Turn);
                if(!AnyMovesPossible())
                    Finish();
            }

            return laidDiscsCount;
        }

        /// <summary>
        /// Perform a move from a position into a single direction.
        /// </summary>
        /// <param name="movePosition"></param>
        /// <param name="directionDelegate"></param>
        /// <returns>how many discs were laid. 0 means invalid move</returns>
        private int DoMove(Vector2 movePosition, DirectionDelegate directionDelegate)
        {
            // must be within bounds
            if (movePosition.Y < 0 || movePosition.Y >= Board.GetLength(0) || movePosition.X < 0 || movePosition.X >= Board.GetLength(1))
                return 0;

            // get all tiles in direction
            IList<Vector2> positions = ValidPositionInDirection(movePosition, directionDelegate);
            if (positions.Count == 0)
                return 0;

            // set colours
            foreach (Vector2 position in positions)
            {
                Board[position.Y, position.X] = Turn;
            }
            Board[movePosition.Y, movePosition.X] = Turn;

            return positions.Count;
        }

        /// <summary>
        /// Finish the game and mark the winner
        /// </summary>
        private void Finish()
        {
            IsFinished = true;
            Winner = DominantColour();
            Turn = Colour.None;
        }

        /// <summary>
        /// Check which colour occurs most often on the board
        /// </summary>
        /// <returns>colour with most places on the board. If equal returns Kleur.Geen</returns>
        public Colour DominantColour()
        {
            // count how often black and white occur
            int black = 0;
            int white = 0;
            for (int row = 0; row < Board.GetLength(0); ++row)
            {
                for (int column = 0; column < Board.GetLength(1); ++column)
                {
                    if (Board[row, column] == Colour.Black)
                    {
                        black++;
                    }
                    else if (Board[row, column] == Colour.White)
                    {
                        white++;
                    }
                }
            }

            // return most occuring colour
            // or Kleur.None if equal
            if (black > white)
                return Colour.Black;
            else if (white > black)
                return Colour.White;
            else
                return Colour.None;
        }

        /// <summary>
        /// End turn and give turn to the other player
        /// </summary>
        /// <returns>if there are still any turns possible for either players</returns>
        public bool Pass()
        {
            bool movesPossible1 = AnyMovesPossible();
            Turn = OppositeColour(Turn);
            return movesPossible1 || AnyMovesPossible();
        }

        /// <summary>
        /// If a move is possible for given position
        /// </summary>
        /// <param name="rowMove">Y position (row)</param>
        /// <param name="columnMove">X position (column)</param>
        /// <returns></returns>
        public bool MovePossible(int rowMove, int columnMove)
        {
            // must be within bounds
            if (rowMove < 0 || rowMove >= Board.GetLength(0) || columnMove < 0 || columnMove >= Board.GetLength(1))
                throw new MoveOutOfBoundsException(new Vector2(columnMove, rowMove));

            if (Board[rowMove, columnMove] != Colour.None)
                return false;

            // check for all directions
            return _directionDelegates.Any(directionDelegate => PossibleMove(new Vector2(columnMove, rowMove), directionDelegate));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="movePosition">position where to check to place</param>
        /// <param name="directionDelegate">which direction to check for</param>
        /// <returns></returns>
        private bool PossibleMove(Vector2 movePosition, DirectionDelegate directionDelegate)
        {
            // must be empty
            if (Board[movePosition.Y, movePosition.X] != Colour.None)
                return false;

            return ValidPositionInDirection(movePosition, directionDelegate).Count > 0;
        }

        /// <summary>
        /// Get all valid positions in a given direction.
        /// Only returns the positions that would be flipped if a move were to be made
        /// </summary>
        /// <param name="movePosition"></param>
        /// <param name="directionDelegate"></param>
        /// <returns>valid positions for given move</returns>
        private IList<Vector2> ValidPositionInDirection(Vector2 movePosition, DirectionDelegate directionDelegate)
        {
            IList<Vector2> positions = new List<Vector2>();

            // get all tiles in direction
            IList<Vector2> positionsInDirection = PositionsInDirection(movePosition, directionDelegate);

            Colour opponentColour = OppositeColour(Turn);
            for (int i = 0; i < positionsInDirection.Count; ++i)
            {
                Vector2 position = positionsInDirection[i];
                Colour thisColour = Board[position.Y, position.X];

                // first colour has to be the opponent's colour colour
                if (i == 0)
                {
                    if (thisColour != opponentColour)
                    {
                        break;
                    }
                    positions.Add(position);
                }
                // if it reached the last one, and its still not the same colour
                // it must be invalid
                else if (i == positionsInDirection.Count - 1 && thisColour != Turn)
                {
                    break;
                }
                else
                {
                    positions.Add(position);

                    // once it reaches their own colour again we know it's a valid move,
                    // because the opposite colour is sandwiched between this own colour
                    if (thisColour == Turn)
                    {
                        return positions;
                    }
                    // however if it reaches an empty tile it's invalid,
                    // because the opposite colour sandwiched between this colour and an empty tile is an invalid move
                    else if (thisColour == Colour.None)
                    {
                        break;
                    }
                }
            }

            // if invalid an empty list must be returned
            return new List<Vector2>();
        }

        /// <summary>
        /// invert colour
        /// </summary>
        /// <param name="colour">colour to get the opposite of</param>
        /// <returns></returns>
        private Colour OppositeColour(Colour colour)
        {
            return colour == Colour.Black ? Colour.White : Colour.Black;
        }

        /// <summary>
        /// Give a starter position and a direction. Get a list of all positions in that direction.
        /// </summary>
        /// <param name="row">start position Y</param>
        /// <param name="column">start position X</param>
        /// <param name="directionDelegate">which direction to use</param>
        /// <returns></returns>
        private IList<Vector2> PositionsInDirection(Vector2 startPosition, DirectionDelegate directionDelegate)
        {
            Vector2 position = startPosition.Clone() as Vector2;
            IList<Vector2> positions = new List<Vector2>();
            while (directionDelegate(position) == true)
            {
                positions.Add(new Vector2(position.X, position.Y));
            }

            return positions;
        }


        /// <summary>
        /// Go up 1 position using rows and column
        /// </summary>
        /// <param name="row">current row position</param>
        /// <param name="column">current column position</param>
        /// <returns>true if possible, false if out of bounds</returns>
        private bool DirectionTop(Vector2 position)
        {
            position.Y--;
            return position.Y >= 0;
        }
        /// <summary>
        /// Go top right 1 position using position.Ys and position.X
        /// </summary>
        /// <param name="position.Y">current position.Y position</param>
        /// <param name="position.X">current position.X position</param>
        /// <returns>true if possible, false if out of bounds</returns>
        private bool DirectionTopRight(Vector2 position)
        {
            position.Y--;
            position.X++;
            return position.Y >= 0 && position.X < Board.GetLength(1);
        }
        /// <summary>
        /// Go right 1 position using position.Ys and position.X
        /// </summary>
        /// <param name="position.Y">current position.Y position</param>
        /// <param name="position.X">current position.X position</param>
        /// <returns>true if possible, false if out of bounds</returns>
        private bool DirectionRight(Vector2 position)
        {
            position.X++;
            return position.X < Board.GetLength(1);
        }
        /// <summary>
        /// Go bottom right 1 position using position.Ys and position.X
        /// </summary>
        /// <param name="position.Y">current position.Y position</param>
        /// <param name="position.X">current position.X position</param>
        /// <returns>true if possible, false if out of bounds</returns>
        private bool DirectionBottomRight(Vector2 position)
        {
            position.Y++;
            position.X++;
            return position.Y < Board.GetLength(0) && position.X < Board.GetLength(1);
        }
        /// <summary>
        /// Go down 1 position using position.Ys and position.X
        /// </summary>
        /// <param name="position.Y">current position.Y position</param>
        /// <param name="position.X">current position.X position</param>
        /// <returns>true if possible, false if out of bounds</returns>
        private bool DirectionBottom(Vector2 position)
        {
            position.Y++;
            return position.Y < Board.GetLength(0);
        }
        /// <summary>
        /// Go bottom left 1 position using position.Ys and position.X
        /// </summary>
        /// <param name="position.Y">current position.Y position</param>
        /// <param name="position.X">current position.X position</param>
        /// <returns>true if possible, false if out of bounds</returns>
        private bool DirectionBottomLeft(Vector2 position)
        {
            position.Y++;
            position.X--;
            return position.Y < Board.GetLength(0) && position.X > 0;
        }
        /// <summary>
        /// Go left 1 position using position.Ys and position.X
        /// </summary>
        /// <param name="position.Y">current position.Y position</param>
        /// <param name="position.X">current position.X position</param>
        /// <returns>true if possible, false if out of bounds</returns>
        private bool DirectionLeft(Vector2 position)
        {
            position.X--;
            return position.X >= 0;
        }
        /// <summary>
        /// Go top left 1 position using position.Ys and position.X
        /// </summary>
        /// <param name="position.Y">current position.Y position</param>
        /// <param name="position.X">current position.X position</param>
        /// <returns>true if possible, false if out of bounds</returns>
        private bool DirectionTopLeft(Vector2 position)
        {
            position.Y--;
            position.X--;
            return position.Y > 0 && position.X > 0;
        }
    }
}

﻿namespace ReversiRestApi.Models
{
    public enum Colour { None, White, Black };

    public interface IGame
    {
        string Token { get; set; }
        string Description { get; set; }
        string Player1Token { get; set; }
        string Player2Token { get; set; }
        Colour[,] Board { get; set; }
        Colour Turn { get; set; }
        bool IsFinished { get; set; }
        Colour Winner { get; set; }

        bool Pass();
        bool AnyMovesPossible();

        /// <summary>
        /// Which colour occurs most often
        /// </summary>
        /// <returns></returns>
        Colour DominantColour();

        /// <summary>
        /// Check if a move is possible at a specific position
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <returns></returns>
        bool MovePossible(int row, int column);

        /// <summary>
        /// Attempt to perform a move for the current Turn colour
        /// </summary>
        /// <param name="row">Y position to place</param>
        /// <param name="column">X position to place</param>
        /// <returns>the amount of discs that were conquered. 0 means it was an invalid move and no move was performed</returns>
        int DoMove(int row, int column);
    }
}

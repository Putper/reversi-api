﻿namespace ReversiRestApi.Models
{
    public class DoMoveRequest
    {
        public string PlayerToken { get; set; } = null;
        public int Row { get; set; }
        public int Column { get; set; }
    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ReversiRestApi.Models
{
    public class TurnDatum
    {
        /// <summary>
        /// The token of the game this turn belongs to
        /// </summary>
        [Required]
        public string GameToken { get; set; }

        /// <summary>
        /// which turn this is in the game.
        /// Starts with turn 0
        /// </summary>
        [Required]
        public int Turn { get; set; }

        /// <summary>
        /// which player performed this turn.
        /// value=false for player 1,
        /// value=true for player 2
        /// </summary>
        public bool IsPlayer2 { get; set; }

        /// <summary>
        /// How many discs were conquered/taken/defeated in this turn
        /// </summary>
        public int ConqueredDiscs { get; set; }

        /// <summary>
        /// How many discs player1 has in ownership at the end of this turn
        /// </summary>
        public int Player1OwnedDiscs { get; set; }

        /// <summary>
        /// How many discs player2 has in ownership at the end of this turn
        /// </summary>
        public int Player2OwnedDiscs { get; set; }


        //[ForeignKey(nameof(GameToken))]
        //public SerialisableGame Game { get; set; }
    }
}

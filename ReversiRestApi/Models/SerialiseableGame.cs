﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace ReversiRestApi.Models
{
    // TODO: not all properties should be set-able from the API.
    public class SerialisableGame
    {
        [Key]
        public string Token { get; set; }
        public string Description { get; set; }
        [Required]
        public string Player1Token { get; set; }
        public string Player2Token { get; set; }
        /// <summary>
        /// a string of numbers representing the board
        /// 0 = unoccupied
        /// 1 = white (player1)
        /// 2 = black (player2)
        /// 3 = new row
        /// </summary>
        public string Board { get; set; }
        public Colour Turn { get; set; }
        public bool IsFinished { get; set; } = false;
        public Colour Winner { get; set; } = Colour.None;
        public ICollection<TurnDatum> TurnData { get; set; }

        public SerialisableGame()
        { }

        public void RandomToken()
        {
            Token = Guid.NewGuid().ToString();
        }

        public void RandomTurn()
        {
            Random random = new Random();
            Turn = random.NextDouble() >= 0.5 ? Colour.Black : Colour.White;
        }

        public void ResetBoard()
        {
            Board = "000000003" +
                "000000003" +
                "000000003" +
                "000120003" +
                "000210003" +
                "000000003" +
                "000000003" +
                "00000000";
        }

        /// <summary>
        /// convert this serialisable game class to a regular game class
        /// </summary>
        /// <returns></returns>
        public Game ToGame()
        {
            Game game = new Game()
            {
                Token = Token,
                Description = Description,
                Player1Token = Player1Token,
                Player2Token = Player2Token,
                Turn = Turn,
                Winner = Winner,
            };

            game.Board = StringToBoard();

            return game;
        }


        /// <summary>
        /// Create a serialisable game from regular game class
        /// </summary>
        /// <param name="game">game to create from</param>
        /// <returns></returns>
        public static SerialisableGame FromGame(Game game)
        {
            if (game == null)
            {
                return null;
            }
            var serialisableGame = new SerialisableGame();
            serialisableGame.UpdateDataFrom(game);
            return serialisableGame;
        }

        /// <summary>
        /// Replace data in current object with data from Game
        /// </summary>
        /// <param name="game"></param>
        public void UpdateDataFrom(Game game)
        {
            string board = game.Board != null
                ? BoardToString(game.Board)
                : null;

            Token = game.Token;
            Description = game.Description;
            Player1Token = game.Player1Token;
            Player2Token = game.Player2Token;
            Board = board;
            Turn = game.Turn;
            IsFinished = game.IsFinished;
            Winner = game.Winner;
        }

        /// <summary>
        /// convert a board to a string of numbers
        /// 0 = empty, 1 = white, 2 = black, 3 = new row
        /// </summary>
        /// <param name="board"></param>
        /// <returns></returns>
        private static string BoardToString(Colour[,] board)
        {
            if (board == null)
            {
                return "";
            }

            StringBuilder stringBoard = new StringBuilder();

            int boardLength = board.GetLength(0);
            for (int row = 0; row < boardLength; ++row)
            {
                for (int column = 0; column < board.GetLength(1); ++column)
                {
                    int tile = (int)board[row, column];
                    stringBoard.Append(tile);
                }
                // new rows are represented with a 3
                if (row != boardLength-1)
                    stringBoard.Append("3");
            }

            return stringBoard.ToString();
        }

        /// <summary>
        /// Converts a string to a Colour[,].
        /// 0 = empty, 1 = white, 2 = black, 3 = new row
        /// </summary>
        /// <param name="stringBoard"></param>
        /// <returns></returns>
        private Colour[,] StringToBoard()
        {
            string stringBoard = Board;
            if (String.IsNullOrEmpty(stringBoard))
            {
                return null;
            }

            string[] rows = stringBoard.Split("3");
            Colour[,] board = new Colour[rows.Length, rows[0].Length];

            for (int row = 0; row < rows.Length; ++row)
            {
                for (int column = 0; column < rows[row].Length; ++column)
                {
                    int number = (int)Char.GetNumericValue(rows[row][column]);
                    Colour colour = (Colour)number;
                    board[row, column] = colour;
                }
            }

            return board;
        }

        ///// <summary>
        ///// convert Bord to a colour array usable in the Game clas
        ///// </summary>
        ///// <returns></returns>
        //public Colour[,] BoardToColourArray()
        //{
        //    if (Board == null)
        //    {
        //        return null;
        //    }

        //    Colour[,] board = new Colour[Board.Length, Board[0].Length];
        //    for (int i = 0; i < Board.Length; ++i)
        //    {
        //        for (int j = 0; j < Board[i].Length; ++j)
        //        {
        //            board[i, j] = Enum.Parse<Colour>(Board[i][j]);
        //        }
        //    }
        //    return board;
        //}

        ///// <summary>
        ///// Convert the regular enum board to a string jagged array
        ///// seperated to make public methods clearer.
        ///// </summary>
        ///// <param name="board"></param>
        ///// <returns></returns>
        //public static string[][] BoardToJaggedArray(Colour[,] board)
        //{
        //    if(board == null)
        //    {
        //        return null;
        //    }

        //    string[][] rows = new string[board.GetLength(0)][];
        //    for (int i = 0; i < board.GetLength(0); ++i)
        //    {
        //        string[] row = new string[board.GetLength(1)];
        //        for (int j = 0; j < board.GetLength(1); ++j)
        //        {
        //            string colour = board[i, j].ToString();
        //            row[j] = colour;
        //        }
        //        rows[i] = row;
        //    }
        //    return rows;
        //}

        //private static string BoardToJson(Kleur[,] board)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    sb.Append("[");
        //    for (int i = 0; i < board.GetLength(0); ++i)
        //    {
        //        sb.Append("[");
        //        for (int j = 0; j < board.GetLength(1); ++j)
        //        {
        //            string colour = board[i, j].ToString();
        //            string columnComma = (j < board.GetLength(1) - 1) ? "," : "";
        //            sb.Append($"\"{colour}\"{columnComma}");
        //        }
        //        string rowComma = (i < board.GetLength(0) - 1) ? "," : "";
        //        sb.Append("]" + rowComma);
        //    }
        //    sb.Append("]");

        //    return sb.ToString();
        //}
    }
}

﻿namespace ReversiRestApi.Models
{
    public class DoMoveResponse
    {
        public bool Success { get; set; }
        public SerialisableGame Game { get; set; }

        public DoMoveResponse(bool success, SerialisableGame game)
        {
            Success = success;
            Game = game;
        }
    }
}

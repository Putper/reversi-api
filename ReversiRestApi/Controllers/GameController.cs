﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ReversiRestApi.DAL;
using ReversiRestApi.Exceptions;
using ReversiRestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using ReversiRestApi.Attributes;

namespace ReversiRestApi.Controllers
{
    [Route("api/game")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly IGameRepository repository;
        public GameController(IGameRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// GET api/game/{playertoken}
        /// Get the games the player is participating in
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [SecretRequired]
        public ActionResult<IEnumerable<SerialisableGame>> GetGames([FromQuery] string playerToken = "")
        {
            var games = GetGamesForPlayer(playerToken);
            return new ObjectResult(games);
        }

        /// <summary>
        /// GET api/game/{token}
        /// Get specific game with token
        /// </summary>
        /// <param name="gameToken"></param>
        /// <returns></returns>
        [HttpGet("{gameToken}")]
        [Authorise]
        public ActionResult<SerialisableGame> GetGame(string gameToken)
        {
            return repository.GetGame(gameToken);
        }

        /// <summary>
        /// Get who's turn it is, for the given gameToken
        /// </summary>
        /// <param name="gameToken"></param>
        /// <returns></returns>
        [HttpGet("{gameToken}/turn")]
        [PlayersOnly]
        public ActionResult<int> GetTurn(string gameToken)
        {
            SerialisableGame game = repository.GetGame(gameToken);
            if (game == null)
            {
                return NotFound();
            }
            return (int)game.Turn;
        }

        /// <summary>
        /// Check if game has found a second player or not
        /// </summary>
        /// <param name="gameToken"></param>
        /// <returns></returns>
        [HttpGet("{gameToken}/ready")]
        [PlayersOnly]
        public ActionResult<bool> GetGameHasFoundPlayer(string gameToken)
        {
            var games = GetGamesLookingForPlayersInternal();
            var game = games.Where(game => game.Token == gameToken).FirstOrDefault();
            return game == null;
        }

        /// <summary>
        /// GET api/game/looking
        /// get all games that are looking for a player
        /// </summary>
        /// <returns></returns>
        [Route("looking")]
        [HttpGet]
        [SecretRequired]
        public ActionResult<IEnumerable<SerialisableGame>> GetGamesLookingForPlayers()
        {
            IEnumerable<SerialisableGame> games = GetGamesLookingForPlayersInternal();
            return new ObjectResult(games);
        }

        /// <summary>
        /// GET api/game/looking/descriptions
        /// get all descriptions for games that are looking for a player
        /// </summary>
        /// <returns>a list of descriptions</returns>
        [Route("looking/descriptions")]
        [HttpGet]
        [SecretRequired]
        public ActionResult<IEnumerable<string>> GetDescriptionsOfGamesLookingForPlayers()
        {
            IEnumerable<SerialisableGame> games = GetGamesLookingForPlayersInternal();
            IList<string> descriptions = new List<string>();
            foreach(var game in games)
            {
                descriptions.Add(game.Description);
            }
            return new ObjectResult(descriptions);
        }

        /// <summary>
        /// POST api/game
        /// create a game
        /// </summary>
        /// <param name="game"></param>
        /// <returns></returns>
        [HttpPost]
        [SecretRequired]
        public ActionResult<SerialisableGame> CreateGame([Bind("Description,Player1Token")] SerialisableGame game)
        {
            if (GetUnfinishedGameForPlayer(game.Player1Token) != null)
                throw new Exception("Player already has an ongoing game");

            game.RandomTurn();
            game.RandomToken();
            game.ResetBoard();
            game.IsFinished = false;
            repository.AddGame(game);
            repository.Save();
            return Created("api/game", game);
        }

        /// <summary>
        /// PUT api/game/{token}/join
        /// </summary>
        /// <param name="gameToken">token of game to join</param>
        /// <param name="playerToken">token of player who wants to join</param>
        /// <returns>game that has been joined</returns>
        /// <exception cref="Exception"></exception>
        [HttpPut("{gameToken}/join")]
        [SecretRequired]
        public ActionResult<SerialisableGame> Join(string gameToken, [FromBody] string playerToken)
        {
            var sGame = repository.GetGame(gameToken);
            if (sGame == null)
                throw new Exception("gameToken is not a token for an existing game");
            if (sGame.Player1Token != null && sGame.Player2Token != null)
                throw new Exception("Game already contains two players");
            if (sGame.Player1Token == playerToken || sGame.Player2Token == playerToken)
                throw new Exception("Player is already a part of this game");

            if (GetUnfinishedGameForPlayer(playerToken) != null)
                throw new Exception("Player already has an ongoing game");
            
            sGame.Player2Token = playerToken;
            repository.Save();

            return sGame;
        }

        /// <summary>
        /// PUT api/game/{token}/surrender
        /// surrender game
        /// </summary>
        /// <param name="gameToken"></param>
        /// <param name="playerToken"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpPut("{gameToken}/surrender")]
        [SecretRequired]
        public ActionResult<SerialisableGame> Surrender(string gameToken, [FromBody] string playerToken)
        {
            var sGame = repository.GetGame(gameToken);
            if (sGame == null)
                throw new Exception("gameToken is not a token for an existing game");

            sGame.IsFinished = true;

            // mark opponent as winner
            sGame.Winner = (sGame.Player1Token == playerToken)
                ? Colour.Black
                : Colour.White;

            repository.Save();
            return sGame;
        }


        /// <summary>
        /// DELETE api/game/{token}
        /// surrender game
        /// </summary>
        /// <param name="gameToken"></param>
        /// <param name="playerToken"></param>
        /// <returns></returns>
        /// <exception cref="Exception"></exception>
        [HttpDelete("{gameToken}")]
        [SecretRequired]
        public ActionResult Delete(string gameToken, [FromBody] string playerToken)
        {
            var sGame = repository.GetGame(gameToken);
            if (sGame == null)
                throw new Exception("gameToken is not a token for an existing game");

            if (sGame.Player1Token != playerToken && sGame.Player2Token != playerToken)
                throw new Exception("Must be player in game");

            if (sGame.Player1Token != null && sGame.Player2Token != null)
                throw new Exception("Can only delete games still looking for players. Surrender instead");

            repository.DeleteGame(gameToken);
            repository.Save();

            return StatusCode(200);
        }

        /// <summary>
        /// PUT api/game/{token}
        /// Do move for a given game
        /// </summary>
        /// <param name="gameToken">game to do the move for</param>
        /// <param name="playerToken">player performing the move</param>
        /// <param name="row">row location of the move</param>
        /// <param name="column">column location of the move</param>
        /// <returns></returns>
        /// <exception cref="Exception">If gametoken is invalid, player is not part of the game or it's not the turn of the given player</exception>
        [HttpPut("{gameToken}")]
        [PlayersOnly]
        public ActionResult<DoMoveResponse> DoMove(string gameToken, [FromBody] DoMoveRequest model)
        {
            string playerToken = model.PlayerToken;
            int row = model.Row;
            int column = model.Column;
            SerialisableGame sGame = repository.GetGame(gameToken);

            // game must exist
            if(sGame == null)
                throw new Exception("gameToken invalid: not a token for an existing game");
            // player must be part of this game
            if(sGame.Player1Token != playerToken && sGame.Player2Token != playerToken)
                throw new Exception("Player is not playing in the given game");
            // must be player's turn
            if(sGame.Player1Token == playerToken && sGame.Turn != Colour.White
                || sGame.Player2Token == playerToken && sGame.Turn != Colour.Black)
                throw new Exception("It is not the turn of this player");

            Game game = sGame.ToGame();
            try
            {
                bool isPlayer2Turn = game.Turn == Colour.Black;

                // perform move
                int laidDiscs = game.DoMove(row, column);
                sGame.UpdateDataFrom(game);

                // -1 because the final laid disc is not conquered
                int conqueredDiscs = laidDiscs - 1;

                // save data for this turn
                TurnDatum turnDatum = new()
                {
                    GameToken = gameToken,
                    IsPlayer2 = isPlayer2Turn,
                    ConqueredDiscs = conqueredDiscs,
                    // TODO: this can be optimised by counting once for both
                    Player1OwnedDiscs = sGame.Board.Count(c => c == '1'),
                    Player2OwnedDiscs = sGame.Board.Count(c => c == '2'),
                };
                repository.AddTurnDatum(turnDatum);

                repository.Save();

                return new DoMoveResponse(true, sGame);
            }
            catch(Exception e) when(e is MoveNotPossibleException || e is MoveOutOfBoundsException)
            {
                return new DoMoveResponse(false, null);
            }
        }



        /// <summary>
        /// Get games for given player
        /// </summary>
        /// <param name="playerToken"></param>
        /// <returns></returns>
        private IEnumerable<SerialisableGame> GetGamesForPlayer(string playerToken)
        {
            IEnumerable<SerialisableGame> games = repository.GetGames();
            if (!String.IsNullOrEmpty(playerToken))
            {
                games = games.Where(g => g.Player1Token == playerToken || g.Player2Token == playerToken);
            }
            return games;
        }

        /// <summary>
        /// if player has an ongoing game, returns that game
        /// </summary>
        /// <param name="playerToken"></param>
        /// <returns></returns>
        private SerialisableGame GetUnfinishedGameForPlayer(string playerToken)
        {
            return GetGamesForPlayer(playerToken).Where(g => g.IsFinished == false).FirstOrDefault();
        }

        /// <summary>
        /// Get a list of all games looking for players
        /// </summary>
        /// <returns></returns>
        private IEnumerable<SerialisableGame> GetGamesLookingForPlayersInternal()
        {
            var games = repository.GetGames();
            return games.Where(g => String.IsNullOrEmpty(g.Player1Token) || String.IsNullOrEmpty(g.Player2Token));
        }
    }
}

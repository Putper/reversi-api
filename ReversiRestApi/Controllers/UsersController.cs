﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ReversiRestApi.Attributes;
using ReversiRestApi.Models;
using ReversiRestApi.Services;

namespace ReversiRestApi.Controllers
{
    [ApiController]
    [Route("api/users")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost("token")]
        [SecretRequired]
        public string GenerateToken(GenerateTokenRequest request)
        {
            return _userService.GenerateJwtToken(request.ToUser());
        }
    }
}

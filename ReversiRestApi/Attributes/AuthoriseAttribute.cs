﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ReversiRestApi.Models;
using System;

namespace ReversiRestApi.Attributes
{
    /// <summary>
    /// Checks if is authorised. Can be a player with any role, or the MVC server using a secret token.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthoriseAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = (User)context.HttpContext.Items["User"];
            // not authorised using player token
            if (user == null)
            {
                // not MVC server either
                var isMVCServer = context.HttpContext.Items["IsMVCServer"];
                if (isMVCServer == null || (bool)isMVCServer == false)
                    context.Result = new JsonResult(new { message = "Unauthorised" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }
        }
    }
}

﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using ReversiRestApi.Models;
using System;
using System.Text;

namespace ReversiRestApi.Attributes
{
    /// <summary>
    /// The MVC app uses a secret token to authenticate. this attribute checks if it is that token.
    /// https://en.wikipedia.org/wiki/Shared_secret
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class SecretRequiredAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var isMVCServer = context.HttpContext.Items["IsMVCServer"];
            if(isMVCServer == null || (bool)isMVCServer == false)
            {
                context.Result = new JsonResult(new { message = "Unauthorised" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }
        }
    }
}

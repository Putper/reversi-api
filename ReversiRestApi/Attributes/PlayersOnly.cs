﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;
using System;
using ReversiRestApi.Models;

namespace ReversiRestApi.Attributes
{
    /// <summary>
    /// players are given a JWT token, which they use to do requests to this API.
    /// This checks if they are indeed a player sending a valid JWT token.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public class PlayersOnly : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = (User)context.HttpContext.Items["User"];
            if (user == null)
                context.Result = new JsonResult(new { message = "Unauthorised" }) { StatusCode = StatusCodes.Status401Unauthorized };
        }
    }
}

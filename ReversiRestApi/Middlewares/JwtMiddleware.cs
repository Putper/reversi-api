﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ReversiRestApi.Models;
using ReversiRestApi.Services;
using System.Linq;
using System.Threading.Tasks;

namespace ReversiRestApi.Middlewares
{
    /// <summary>
    /// Sets User in the middleware
    /// Source: https://jasonwatmore.com/post/2021/04/30/net-5-jwt-authentication-tutorial-with-example-api#jwt-middleware-cs
    /// </summary>
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;

        public JwtMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IUserService userService)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            if(token != null && userService.IsJwtToken(token))
            {
                User user = userService.ProcessJwtToken(token);
                context.Items["User"] = user;

                //var identity = (ClaimsIdentity)context.User.Identity;
                //identity.AddClaim(new Claim("UserId", user.Id));
            }

            await _next(context);
        }
    }
}

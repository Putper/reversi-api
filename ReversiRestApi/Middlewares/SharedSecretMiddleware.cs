﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using ReversiRestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReversiRestApi.Middlewares
{
    /// <summary>
    /// sets a boolean "IsMVCServer" in the context.
    /// Because the MVC server authenticates by sending the shared secret
    /// </summary>
    public class SharedSecretMiddleware
    {
        private readonly RequestDelegate _next;

        public SharedSecretMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context, IOptions<Secrets> secrets)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            context.Items["IsMVCServer"] = (token != null && token == secrets.Value.Shared);

            await _next(context);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using ReversiRestApi.Models;
using System.Collections.Generic;
using System.Linq;

namespace ReversiRestApi.DAL
{
    public class GameRepository : IGameRepository
    {
        private ReversiContext _dataContext { get; set; }

        public GameRepository(ReversiContext dataContext)
        {
            this._dataContext = dataContext;
        }

        public bool AddGame(SerialisableGame game)
        {
            _dataContext.Games.Add(game);
            return true;
        }

        public SerialisableGame GetGame(string gameToken)
        {
            return _dataContext.Games
                .Include(game => game.TurnData)
                .FirstOrDefault(game => game.Token == gameToken);
        }

        public IEnumerable<SerialisableGame> GetGames()
        {
            return _dataContext.Games.ToList();
        }

        public void DeleteGame(string gameToken)
        {
            _dataContext.Games.Remove(GetGame(gameToken));
        }


        /// <summary>
        /// TurnDatum is a composite primary key.
        /// Each TurnDatum represents 1 turn in a game, bound with the `GameToken`.
        /// The `Turn` property is the turn number in the game, which is automatically incremented
        /// by this method.
        /// </summary>
        /// <param name="datum"></param>
        /// <returns></returns>
        public bool AddTurnDatum(TurnDatum datum)
        {
            // get last TurnDatum for this game
            TurnDatum lastTurn = _dataContext.TurnData
                .Where(turn => turn.GameToken == datum.GameToken)
                .OrderByDescending(turn => turn.Turn)
                .FirstOrDefault();

            // increment Turn property
            datum.Turn = (lastTurn == null) ? 0 : lastTurn.Turn + 1;

            _dataContext.TurnData.Add(datum);
            return true;
        }

        public IEnumerable<TurnDatum> GetTurnData(string gameToken)
        {
            return _dataContext.TurnData.Where(turn => turn.GameToken == gameToken).ToList();
        }


        public void Save()
        {
            _dataContext.SaveChanges();
        }
    }
}

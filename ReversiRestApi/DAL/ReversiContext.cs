﻿using Microsoft.EntityFrameworkCore;
using ReversiRestApi.Models;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

namespace ReversiRestApi.DAL
{
    public class ReversiContext : DbContext
    {
        public ReversiContext(DbContextOptions options) : base(options)
        { }

        public DbSet<SerialisableGame> Games { get; set; }
        public DbSet<TurnDatum> TurnData { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // TurnDatum composite primary key
            modelBuilder.Entity<TurnDatum>()
                .HasKey(cd => new { cd.GameToken, cd.Turn });

            // TurnDatum is bound to a game
            //modelBuilder.Entity<TurnDatum>()
            //    .HasOne(cd => cd.Game)
            //    .WithMany(game => game.TurnData)
            //    .HasForeignKey(cd => cd.GameToken);

            modelBuilder.Entity<SerialisableGame>()
                .HasMany(game => game.TurnData)
                //.WithOne(turn => turn.Game)
                .WithOne()
                .HasForeignKey(turn => turn.GameToken);
        }
    }
}

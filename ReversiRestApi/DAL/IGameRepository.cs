﻿using ReversiRestApi.Models;
using System.Collections.Generic;

namespace ReversiRestApi.DAL
{
    public interface IGameRepository
    {
        bool AddGame(SerialisableGame game);
        SerialisableGame GetGame(string gameToken);
        IEnumerable<SerialisableGame> GetGames();
        void DeleteGame(string gameToken);

        bool AddTurnDatum(TurnDatum datum);
        IEnumerable<TurnDatum> GetTurnData(string gameToken);

        void Save();
    }
}

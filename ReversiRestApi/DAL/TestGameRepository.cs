﻿using ReversiRestApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReversiRestApi.DAL
{
    public class TestGameRepository : IGameRepository
    {
        public List<SerialisableGame> Games { get; set; }

        public TestGameRepository()
        {
            SerialisableGame spel1 = new SerialisableGame();
            SerialisableGame spel2 = new SerialisableGame();
            SerialisableGame spel3 = new SerialisableGame();
            spel1.Player1Token = "abcdef";
            spel1.Description = "Potje snel reveri, dus niet lang nadenken";
            spel2.Player1Token = "ghijkl";
            spel2.Player2Token = "mnopqr";
            spel2.Description = "Ik zoek een gevorderde tegenspeler!";
            spel3.Player1Token = "stuvwx";
            spel3.Description = "Na dit spel wil ik er nog een paar spelen tegen zelfde tegenstander";
            Games = new List<SerialisableGame> { spel1, spel2, spel3 };
        }

        public bool AddGame(SerialisableGame game)
        {
            Games.Add(game);
            return true;
        }

        public IEnumerable<SerialisableGame> GetGames()
        {
            return Games;
        }

        public SerialisableGame GetGame(string gameToken)
        {

            return Games.FirstOrDefault(game => game.Token == gameToken);
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        public void DeleteGame(string gameToken)
        {
            throw new NotImplementedException();
        }

        public bool AddTurnDatum(TurnDatum datum)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<TurnDatum> GetTurnData(string gameToken)
        {
            throw new NotImplementedException();
        }
    }
}

﻿using Microsoft.Extensions.Options;
using ReversiRestApi.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace ReversiRestApi.DAL
{
    public class ADOGameRepository : IGameRepository
    {
        private string _connectionString = "";

        public ADOGameRepository(IOptions<ConnectionStrings> connectionStrings)
        {
            _connectionString = connectionStrings.Value.DefaultConnection;
        }

        public bool AddGame(SerialisableGame game)
        {
            int result;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string query = "INSERT INTO Game VALUES(@Token, @Description, @Player1Token, @Player2Token, @Board, @Turn, @IsFinished)";
                SqlCommand command = new SqlCommand(query, connection);

                object Player2Token = String.IsNullOrEmpty(game.Player2Token) ? DBNull.Value : game.Player2Token;
                object description = String.IsNullOrEmpty(game.Description) ? DBNull.Value : game.Description;
                int isFinished = game.IsFinished ? 1 : 0;

                command.Parameters.AddWithValue("@Token", game.Token);
                command.Parameters.AddWithValue("@Description", description);
                command.Parameters.AddWithValue("@Player1Token", game.Player1Token);
                command.Parameters.AddWithValue("@Player2Token", Player2Token);
                command.Parameters.AddWithValue("@Board", game.Board);
                command.Parameters.AddWithValue("@Turn", (int)game.Turn);
                command.Parameters.AddWithValue("@IsFinished", isFinished);

                connection.Open();
                result = command.ExecuteNonQuery();
                connection.Close();
            }
            return result == 1;
        }

        public bool AddTurnDatum(TurnDatum datum)
        {
            throw new NotImplementedException();
        }

        public void DeleteGame(string gameToken)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get game using Token
        /// </summary>
        /// <param name="gameToken"></param>
        /// <returns></returns>
        public SerialisableGame GetGame(string gameToken)
        {
            SerialisableGame game = null;
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                string query = "SELECT * FROM Game WHERE Token=@Token";
                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@Token", gameToken);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    game = ReaderToGame(reader);
                }

                connection.Close();
            }
            return game;
        }

        public IEnumerable<SerialisableGame> GetGames()
        {
            IList<SerialisableGame> games = new List<SerialisableGame>();

            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                string query = "SELECT * FROM Game";
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {

                    games.Add(ReaderToGame(reader));
                }

                connection.Close();
            }

            return games;
        }

        public IEnumerable<TurnDatum> GetTurnData(string gameToken)
        {
            throw new NotImplementedException();
        }

        public void Save()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// convert SqlDataReader to a Game
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        private SerialisableGame ReaderToGame(SqlDataReader reader)
        {
            return new SerialisableGame()
            {
                Token = reader["Token"].ToString(),
                Description = reader["Description"].ToString(),
                Player1Token = reader["Player1Token"].ToString(),
                Player2Token = reader["Player2Token"].ToString(),
                Turn = (Colour)Convert.ToInt32(reader["Turn"]),
                IsFinished = Convert.ToBoolean(reader["IsFinished"]),
                Board = reader["board"].ToString(),
            };
        }
    }
}

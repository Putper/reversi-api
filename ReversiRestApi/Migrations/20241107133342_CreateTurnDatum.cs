﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ReversiRestApi.Migrations
{
    public partial class CreateTurnDatum : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TurnData",
                columns: table => new
                {
                    GameToken = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Turn = table.Column<int>(type: "int", nullable: false),
                    IsPlayer2 = table.Column<bool>(type: "bit", nullable: false),
                    ConqueredDiscs = table.Column<int>(type: "int", nullable: false),
                    Player1OwnedDiscs = table.Column<int>(type: "int", nullable: false),
                    Player2OwnedDiscs = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TurnData", x => new { x.GameToken, x.Turn });
                    table.ForeignKey(
                        name: "FK_TurnData_Games_GameToken",
                        column: x => x.GameToken,
                        principalTable: "Games",
                        principalColumn: "Token",
                        onDelete: ReferentialAction.Cascade);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TurnData");
        }
    }
}

﻿using ReversiRestApi.Models;
using System.Collections.Generic;

namespace ReversiRestApi.Services
{
    public interface IUserService
    {
        User ProcessJwtToken(string token);
        string GenerateJwtToken(User user);
        bool IsJwtToken(string token);
    }
}

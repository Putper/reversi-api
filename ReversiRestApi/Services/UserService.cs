﻿using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using ReversiRestApi.Models;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json;

namespace ReversiRestApi.Services
{
    public class UserService : IUserService
    {
        private readonly string _jwtSecret;
        private readonly ILogger<UserService> _logger;

        public UserService(IOptions<Secrets> secrets, ILogger<UserService> logger)
        {
            _jwtSecret = secrets.Value.JWT;
            _logger = logger;
        }

        /// <summary>
        /// Generate JWT token for given user
        /// which can be validated with the 
        /// ProcessJwtToken() method
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public string GenerateJwtToken(User user)
        {
            // create identity for user
            var identity = new ClaimsIdentity(new[] { new Claim("id", user.Id) });
            foreach (Roles role in user.Roles)
                identity.AddClaim(new Claim("role", role.ToString()));

            // generate token that is valid for 7 days
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = identity,
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            _logger.LogInformation($"Generated JWT token for user {user.Id}");
            return tokenHandler.WriteToken(token);
        }

        /// <summary>
        /// Turn a JWT token into a User.
        /// Throws exception if invalid.
        /// Valid tokens are generated using GenerateJwtToken() method
        /// </summary>
        /// <param name="token">JWT token</param>
        /// <returns>User class based on given token</returns>
        public User ProcessJwtToken(string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSecret);

            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                // parse token
                var jwtToken = (JwtSecurityToken)validatedToken;
                string userId = jwtToken.Claims.First(c => c.Type.Equals("id")).Value;
                // parse roles
                IList<Roles> roles = new List<Roles>();
                IEnumerable<Claim> roleClaims = jwtToken.Claims.Where(claim => claim.Type.Equals("role"));
                foreach (Claim claim in roleClaims)
                {
                    Roles role = (Roles)Enum.Parse(typeof(Roles), claim.Value);
                    roles.Add(role);
                }
                _logger.LogInformation($"Processed JWT token for user {userId}");
                return new User(userId, roles);
            }
            catch (Exception ex)
            {
                _logger.LogError($"JWT Token validation failed for token \"{token}\": {ex.Message}");
                throw;
            }
        }

        /// <summary>
        /// Does a rough estimate if the given token is a JWT token,
        /// by checking if it has the markup of one.
        /// Does not 100% validate it's a real JWT token,
        /// and does not validate it's given out by us.
        /// </summary>
        public bool IsJwtToken(string token)
        {
            // JWT tokens consist of 3 parts split by a period
            string[] parts = token.Split(".");
            if (parts.Length != 3)
                return false;

            try
            {
                // the first part is the header. it's Base64-encoded json.
                byte[] headerData = Convert.FromBase64String(parts.First());
                //// header should say in it's Typ that it's JWT
                string headerString = Encoding.UTF8.GetString(headerData);
                return headerString.Contains("JWT");
                //JwtHeader header = JsonSerializer.Deserialize<JwtHeader>(headerString);
                //return header.Typ.Equals("JWT");
            }
            catch (Exception)
            {
                // if something goes wrong during decoding of the header
                // we know it's not a JWT token.
                return false;
            }
        }
    }
}

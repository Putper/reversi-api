# Reversi API
By [Jasper van Veenhuizen](https://jasperv.dev/).

**School:** Windesheim University of Applied Sciences  
**Minor:** Web Development  
**Class:** Server Technology

This API stores Reversi game data and
allows the MVC project to get game data
and perform moves.  
All game logic is done here in the API.

- API handling game logic can be found in the [current repository](https://gitlab.com/Putper/reversi-api).
- MVC Application which is the game client, handling player info and serving the front-end can be found [here](https://gitlab.com/Putper/reversi-mvc).
- Resources that provide the MVC app with JavaScript, CSS and images can be found [here](https://gitlab.com/Putper/reversi-resources).


## Setup
In the ReversiRestApi folder, make a copy of `envsettings.json.example` and name it `envsettings.json`.
```bash
$ cp ReversiRestApi/envsettings.json.example ReversiRestApi/envsettings.json
```

Open your newly created `envsettings.json` and set a secure shared secret key by replacing `MY_SECRET`.
```bash
$ vim ReversiRestApi/envsettings.json
```


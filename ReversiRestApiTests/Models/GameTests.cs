﻿using NUnit.Framework;
using ReversiRestApi.Exceptions;
using ReversiRestApi.Models;
using System;

namespace ReversiRestApiTests.Models
{
    [TestFixture]
    public class GameTests
    {
        // geen kleur = 0
        // Wit = 1
        // Zwart = 2

        [Test]
        public void ZetMogelijk__BuitenBord_Exception()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //                     v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            //                     1 <
            // Act
            game.Turn = Colour.White;
            //var actual = game.MovePossible(8, 8);
            Exception ex = Assert.Throws<MoveOutOfBoundsException>(delegate { game.MovePossible(8, 8); });
            Assert.That(ex.Message, Is.EqualTo("Zet (8,8) ligt buiten het bord!"));

            // Assert
        }

        [Test]
        public void ZetMogelijk_StartSituatieZet23Zwart_ReturnTrue()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 2 0 0 0 0  <
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.Black;
            var actual = game.MovePossible(2, 3);
            // Assert
            Assert.IsTrue(actual);
        }

        [Test]
        public void ZetMogelijk_StartSituatieZet23Wit_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 1 0 0 0 0 <
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.White;
            var actual = game.MovePossible(2, 3);
            // Assert
            Assert.IsFalse(actual);
        }


        [Test]
        public void ZetMogelijk_ZetAanDeRandBoven_ReturnTrue()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 3] = Colour.White;
            game.Board[2, 3] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 2 0 0 0 0  <
            // 1   0 0 0 1 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            // Act
            game.Turn = Colour.Black;
            var actual = game.MovePossible(0, 3);
            // Assert
            Assert.IsTrue(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandBoven_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 3] = Colour.White;
            game.Board[2, 3] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 1 0 0 0 0  <
            // 1   0 0 0 1 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.White;
            var actual = game.MovePossible(0, 3);
            // Assert
            Assert.IsFalse(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandBovenEnTotBenedenReedsGevuld_ReturnTrue()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 3] = Colour.White;
            game.Board[2, 3] = Colour.White;
            game.Board[3, 3] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[5, 3] = Colour.White;
            game.Board[6, 3] = Colour.White;
            game.Board[7, 3] = Colour.Black;
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 2 0 0 0 0  <
            // 1   0 0 0 1 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 1 1 0 0 0
            // 5   0 0 0 1 0 0 0 0
            // 6   0 0 0 1 0 0 0 0
            // 7   0 0 0 2 0 0 0 0

            // Act
            game.Turn = Colour.Black;
            var actual = game.MovePossible(0, 3);
            // Assert
            Assert.IsTrue(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandBovenEnTotBenedenReedsGevuld_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 3] = Colour.White;
            game.Board[2, 3] = Colour.White;
            game.Board[3, 3] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[5, 3] = Colour.White;
            game.Board[6, 3] = Colour.White;
            game.Board[7, 3] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 2 0 0 0 0  <
            // 1   0 0 0 1 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 1 1 0 0 0
            // 5   0 0 0 1 0 0 0 0
            // 6   0 0 0 1 0 0 0 0
            // 7   0 0 0 1 0 0 0 0

            // Act
            game.Turn = Colour.Black;
            var actual = game.MovePossible(0, 3);
            // Assert
            Assert.IsFalse(actual);
        }






        [Test]
        public void ZetMogelijk_ZetAanDeRandRechts_ReturnTrue()
        {
            // Arrange
            Game game = new Game();
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 2 0 0 0 0  
            // 1   0 0 0 1 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 1 1 2 <
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            // Act
            game.Turn = Colour.Black;
            var actual = game.MovePossible(4, 7);
            // Assert
            Assert.IsTrue(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandRechts_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 1 0 0 0 0  
            // 1   0 0 0 1 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 1 1 1 <
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.White;
            var actual = game.MovePossible(4, 7);
            // Assert
            Assert.IsFalse(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandRechtsEnTotLinksReedsGevuld_ReturnTrue()
        {
            // Arrange
            Game game = new Game();
            game.Board[4, 0] = Colour.Black;
            game.Board[4, 1] = Colour.White;
            game.Board[4, 2] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[4, 4] = Colour.White;
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0 
            // 4   2 1 1 1 1 1 1 2 <
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.Black;
            var actual = game.MovePossible(4, 7);
            // Assert
            Assert.IsTrue(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandRechtsEnTotLinksReedsGevuld_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            game.Board[4, 0] = Colour.Black;
            game.Board[4, 1] = Colour.White;
            game.Board[4, 2] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[4, 4] = Colour.White;
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 0  

            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   2 1 1 1 1 1 1 1 <
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.White;
            var actual = game.MovePossible(4, 7);
            // Assert
            Assert.IsFalse(actual);
        }


        //     0 1 2 3 4 5 6 7
        //                     
        // 0   0 0 0 0 0 0 0 0  
        // 1   0 0 0 0 0 0 0 0
        // 2   0 0 0 0 0 0 0 0
        // 3   0 0 0 1 2 0 0 0
        // 4   0 0 0 2 1 0 0 0
        // 5   0 0 0 0 0 0 0 0
        // 6   0 0 0 0 0 0 0 0
        // 7   0 0 0 0 0 0 0 0



        [Test]
        public void ZetMogelijk_StartSituatieZet22Wit_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //         v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 1 0 0 0 0 0 <
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.White;
            var actual = game.MovePossible(2, 2);
            // Assert
            Assert.IsFalse(actual);
        }
        [Test]
        public void ZetMogelijk_StartSituatieZet22Zwart_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //         v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 2 0 0 0 0 0 <
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.Black;
            var actual = game.MovePossible(2, 2);
            // Assert
            Assert.IsFalse(actual);
        }


        [Test]
        public void ZetMogelijk_ZetAanDeRandRechtsBoven_ReturnTrue()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 5] = Colour.Black;
            game.Board[1, 6] = Colour.Black;
            game.Board[5, 2] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 1  <
            // 1   0 0 0 0 0 0 2 0
            // 2   0 0 0 0 0 2 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 1 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            // Act
            game.Turn = Colour.White;
            var actual = game.MovePossible(0, 7);
            // Assert
            Assert.IsTrue(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandRechtsBoven_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 5] = Colour.Black;
            game.Board[1, 6] = Colour.Black;
            game.Board[5, 2] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 2  <
            // 1   0 0 0 0 0 0 2 0
            // 2   0 0 0 0 0 2 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 1 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            // Act
            game.Turn = Colour.Black;
            var actual = game.MovePossible(0, 7);
            // Assert
            Assert.IsFalse(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandRechtsOnder_ReturnTrue()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 2] = Colour.Black;
            game.Board[5, 5] = Colour.White;
            game.Board[6, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 2 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 1 0 0
            // 6   0 0 0 0 0 0 1 0
            // 7   0 0 0 0 0 0 0 2 <
            // Act
            game.Turn = Colour.Black;
            var actual = game.MovePossible(7, 7);
            // Assert
            Assert.IsTrue(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandRechtsOnder_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 2] = Colour.Black;
            game.Board[5, 5] = Colour.White;
            game.Board[6, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 0  <
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 2 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 1 0 0
            // 6   0 0 0 0 0 0 1 0
            // 7   0 0 0 0 0 0 0 1
            // Act
            game.Turn = Colour.White;
            var actual = game.MovePossible(7, 7);
            // Assert
            Assert.IsFalse(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandLinksBoven_ReturnTrue()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 1] = Colour.White;
            game.Board[2, 2] = Colour.White;
            game.Board[5, 5] = Colour.Black;
            //     0 1 2 3 4 5 6 7
            //     v
            // 0   2 0 0 0 0 0 0 0  <
            // 1   0 1 0 0 0 0 0 0
            // 2   0 0 1 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 2 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0 
            // Act
            game.Turn = Colour.Black;
            var actual = game.MovePossible(0, 0);
            // Assert
            Assert.IsTrue(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandLinksBoven_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 1] = Colour.White;
            game.Board[2, 2] = Colour.White;
            game.Board[5, 5] = Colour.Black;
            //     0 1 2 3 4 5 6 7
            //     v
            // 0   1 0 0 0 0 0 0 0  <
            // 1   0 1 0 0 0 0 0 0
            // 2   0 0 1 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 2 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0          
            // Act
            game.Turn = Colour.White;
            var actual = game.MovePossible(0, 0);
            // Assert
            Assert.IsFalse(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandLinksOnder_ReturnTrue()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 5] = Colour.White;
            game.Board[5, 2] = Colour.Black;
            game.Board[6, 1] = Colour.Black;
            //     0 1 2 3 4 5 6 7
            //     v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 1 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 2 0 0 0 0 0
            // 6   0 2 0 0 0 0 0 0
            // 7   1 0 0 0 0 0 0 0 <
            // Act
            game.Turn = Colour.White;
            var actual = game.MovePossible(7, 0);
            // Assert
            Assert.IsTrue(actual);
        }

        [Test]
        public void ZetMogelijk_ZetAanDeRandLinksOnder_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 5] = Colour.White;
            game.Board[5, 2] = Colour.Black;
            game.Board[6, 1] = Colour.Black;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 0  <
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 1 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 2 0 0 0 0 0
            // 6   0 2 0 0 0 0 0 0
            // 7   2 0 0 0 0 0 0 0
            // Act
            game.Turn = Colour.Black;
            var actual = game.MovePossible(7, 0);
            // Assert
            Assert.IsFalse(actual);
        }

        //---------------------------------------------------------------------------
        [Test]
        //[ExpectedException(typeof(Exception))]
        public void DoeZet_BuitenBord_Exception()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //                     v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            //                     1 <
            // Act
            game.Turn = Colour.White;
            //game.DoMove(8, 8);
            Exception ex = Assert.Throws<MoveOutOfBoundsException>(delegate { game.DoMove(8, 8); });
            Assert.That(ex.Message, Is.EqualTo("Zet (8,8) ligt buiten het bord!"));

            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.White, game.Turn);
        }

        [Test]
        public void DoeZet_StartSituatieZet23Zwart_ZetCorrectUitgevoerd()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 2 0 0 0 0  <
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.Black;
            game.DoMove(2, 3);
            // Assert
            Assert.AreEqual(Colour.Black, game.Board[2, 3]);
            Assert.AreEqual(Colour.Black, game.Board[3, 3]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.White, game.Turn);
        }

        [Test]
        public void DoeZet_StartSituatieZet23Wit_Exception()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 1 0 0 0 0 <
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.White;
            Exception ex = Assert.Throws<MoveNotPossibleException>(delegate { game.DoMove(2, 3); });
            Assert.That(ex.Message, Is.EqualTo("Zet (2,3) is niet mogelijk!"));

            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.None, game.Board[2, 3]);

            Assert.AreEqual(Colour.White, game.Turn);
        }


        [Test]
        public void DoeZet_ZetAanDeRandBoven_ZetCorrectUitgevoerd()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 3] = Colour.White;
            game.Board[2, 3] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 2 0 0 0 0  <
            // 1   0 0 0 1 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            // Act
            game.Turn = Colour.Black;
            game.DoMove(0, 3);
            // Assert
            Assert.AreEqual(Colour.Black, game.Board[0, 3]);
            Assert.AreEqual(Colour.Black, game.Board[1, 3]);
            Assert.AreEqual(Colour.Black, game.Board[2, 3]);
            Assert.AreEqual(Colour.Black, game.Board[3, 3]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.White, game.Turn);
        }

        [Test]
        public void DoeZet_ZetAanDeRandBoven_Exception()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 3] = Colour.White;
            game.Board[2, 3] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 1 0 0 0 0  <
            // 1   0 0 0 1 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.White;
            Exception ex = Assert.Throws<MoveNotPossibleException>(delegate { game.DoMove(0, 3); });
            Assert.That(ex.Message, Is.EqualTo("Zet (0,3) is niet mogelijk!"));

            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.White, game.Board[1, 3]);
            Assert.AreEqual(Colour.White, game.Board[2, 3]);

            Assert.AreEqual(Colour.None, game.Board[0, 3]);

        }

        [Test]
        public void DoeZet_ZetAanDeRandBovenEnTotBenedenReedsGevuld_ZetCorrectUitgevoerd()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 3] = Colour.White;
            game.Board[2, 3] = Colour.White;
            game.Board[3, 3] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[5, 3] = Colour.White;
            game.Board[6, 3] = Colour.White;
            game.Board[7, 3] = Colour.Black;
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 2 0 0 0 0  <
            // 1   0 0 0 1 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 1 1 0 0 0
            // 5   0 0 0 1 0 0 0 0
            // 6   0 0 0 1 0 0 0 0
            // 7   0 0 0 2 0 0 0 0

            // Act
            game.Turn = Colour.Black;
            game.DoMove(0, 3);
            // Assert
            Assert.AreEqual(Colour.Black, game.Board[0, 3]);
            Assert.AreEqual(Colour.Black, game.Board[1, 3]);
            Assert.AreEqual(Colour.Black, game.Board[2, 3]);
            Assert.AreEqual(Colour.Black, game.Board[3, 3]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);
            Assert.AreEqual(Colour.Black, game.Board[5, 3]);
            Assert.AreEqual(Colour.Black, game.Board[6, 3]);
            Assert.AreEqual(Colour.Black, game.Board[7, 3]);

        }

        [Test]
        public void DoeZet_ZetAanDeRandBovenEnTotBenedenReedsGevuld_Exception()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 3] = Colour.White;
            game.Board[2, 3] = Colour.White;
            game.Board[3, 3] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[5, 3] = Colour.White;
            game.Board[6, 3] = Colour.White;
            game.Board[7, 3] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //           v
            // 0   0 0 0 2 0 0 0 0  <
            // 1   0 0 0 1 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 1 1 0 0 0
            // 5   0 0 0 1 0 0 0 0
            // 6   0 0 0 1 0 0 0 0
            // 7   0 0 0 1 0 0 0 0

            // Act
            game.Turn = Colour.Black;
            Exception ex = Assert.Throws<MoveNotPossibleException>(delegate { game.DoMove(0, 3); });
            Assert.That(ex.Message, Is.EqualTo("Zet (0,3) is niet mogelijk!"));

            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.White, game.Board[4, 3]);

            Assert.AreEqual(Colour.White, game.Board[1, 3]);
            Assert.AreEqual(Colour.White, game.Board[2, 3]);
            Assert.AreEqual(Colour.None, game.Board[0, 3]);
        }

        [Test]
        public void DoeZet_ZetAanDeRandRechts_ZetCorrectUitgevoerd()
        {
            // Arrange
            Game game = new Game();
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 1 1 2 <
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            // Act
            game.Turn = Colour.Black;
            game.DoMove(4, 7);
            // Assert
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);
            Assert.AreEqual(Colour.Black, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 5]);
            Assert.AreEqual(Colour.Black, game.Board[4, 6]);
            Assert.AreEqual(Colour.Black, game.Board[4, 7]);
        }

        [Test]
        public void DoeZet_ZetAanDeRandRechts_Exception()
        {
            // Arrange
            Game game = new Game();
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 1 0 0 0 0  
            // 1   0 0 0 1 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 1 1 1 <
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.White;
            //game.DoMove(4, 7);
            Exception ex = Assert.Throws<MoveNotPossibleException>(delegate { game.DoMove(4, 7); });
            Assert.That(ex.Message, Is.EqualTo("Zet (4,7) is niet mogelijk!"));


            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.White, game.Board[4, 5]);
            Assert.AreEqual(Colour.White, game.Board[4, 6]);
            Assert.AreEqual(Colour.None, game.Board[4, 7]);
        }

        [Test]
        public void DoeZet_ZetAanDeRandRechtsEnTotLinksReedsGevuld_ZetCorrectUitgevoerd()
        {
            // Arrange
            Game game = new Game();
            game.Board[4, 0] = Colour.Black;
            game.Board[4, 1] = Colour.White;
            game.Board[4, 2] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[4, 4] = Colour.White;
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0 
            // 4   2 1 1 1 1 1 1 2 <
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.Black;
            game.DoMove(4, 7);
            // Assert
            Assert.AreEqual(Colour.Black, game.Board[4, 0]);
            Assert.AreEqual(Colour.Black, game.Board[4, 1]);
            Assert.AreEqual(Colour.Black, game.Board[4, 2]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);
            Assert.AreEqual(Colour.Black, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 5]);
            Assert.AreEqual(Colour.Black, game.Board[4, 6]);
            Assert.AreEqual(Colour.Black, game.Board[4, 7]);
        }

        [Test]
        public void DoeZet_ZetAanDeRandRechtsEnTotLinksReedsGevuld_Exception()
        {
            // Arrange
            Game game = new Game();
            game.Board[4, 0] = Colour.Black;
            game.Board[4, 1] = Colour.White;
            game.Board[4, 2] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[4, 4] = Colour.White;
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 0  

            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   2 1 1 1 1 1 1 1 <
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.White;

            Exception ex = Assert.Throws<MoveNotPossibleException>(delegate { game.DoMove(4, 7); });
            Assert.That(ex.Message, Is.EqualTo("Zet (4,7) is niet mogelijk!"));

            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.White, game.Board[4, 3]);

            Assert.AreEqual(Colour.Black, game.Board[4, 0]);
            Assert.AreEqual(Colour.White, game.Board[4, 1]);
            Assert.AreEqual(Colour.White, game.Board[4, 2]);

            Assert.AreEqual(Colour.White, game.Board[4, 5]);
            Assert.AreEqual(Colour.White, game.Board[4, 6]);
            Assert.AreEqual(Colour.None, game.Board[4, 7]);
        }


        //     0 1 2 3 4 5 6 7
        //                     
        // 0   0 0 0 0 0 0 0 0  
        // 1   0 0 0 0 0 0 0 0
        // 2   0 0 0 0 0 0 0 0
        // 3   0 0 0 1 2 0 0 0
        // 4   0 0 0 2 1 0 0 0
        // 5   0 0 0 0 0 0 0 0
        // 6   0 0 0 0 0 0 0 0
        // 7   0 0 0 0 0 0 0 0



        [Test]
        public void DoeZet_StartSituatieZet22Wit_Exception()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //         v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 1 0 0 0 0 0 <
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.White;
            Exception ex = Assert.Throws<MoveNotPossibleException>(delegate { game.DoMove(2, 2); });
            Assert.That(ex.Message, Is.EqualTo("Zet (2,2) is niet mogelijk!"));

            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.None, game.Board[2, 2]);
        }

        [Test]
        public void DoeZet_StartSituatieZet22Zwart_Exception()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //         v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 2 0 0 0 0 0 <
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0

            // Act
            game.Turn = Colour.Black;
            Exception ex = Assert.Throws<MoveNotPossibleException>(delegate { game.DoMove(2, 2); });
            Assert.That(ex.Message, Is.EqualTo("Zet (2,2) is niet mogelijk!"));

            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.None, game.Board[2, 2]);
        }


        [Test]
        public void DoeZet_ZetAanDeRandRechtsBoven_ZetCorrectUitgevoerd()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 5] = Colour.Black;
            game.Board[1, 6] = Colour.Black;
            game.Board[5, 2] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 1  <
            // 1   0 0 0 0 0 0 2 0
            // 2   0 0 0 0 0 2 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 1 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            // Act
            game.Turn = Colour.White;
            game.DoMove(0, 7);
            // Assert
            Assert.AreEqual(Colour.White, game.Board[5, 2]);
            Assert.AreEqual(Colour.White, game.Board[4, 3]);
            Assert.AreEqual(Colour.White, game.Board[3, 4]);
            Assert.AreEqual(Colour.White, game.Board[2, 5]);
            Assert.AreEqual(Colour.White, game.Board[1, 6]);
            Assert.AreEqual(Colour.White, game.Board[0, 7]);
        }

        [Test]
        public void DoeZet_ZetAanDeRandRechtsBoven_Exception()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 5] = Colour.Black;
            game.Board[1, 6] = Colour.Black;
            game.Board[5, 2] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 2  <
            // 1   0 0 0 0 0 0 2 0
            // 2   0 0 0 0 0 2 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 1 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            // Act
            game.Turn = Colour.Black;
            Exception ex = Assert.Throws<MoveNotPossibleException>(delegate { game.DoMove(0, 7); });
            Assert.That(ex.Message, Is.EqualTo("Zet (0,7) is niet mogelijk!"));

            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.Black, game.Board[1, 6]);
            Assert.AreEqual(Colour.Black, game.Board[2, 5]);

            Assert.AreEqual(Colour.White, game.Board[5, 2]);

            Assert.AreEqual(Colour.None, game.Board[0, 7]);
        }

        [Test]
        public void DoeZet_ZetAanDeRandRechtsOnder_ZetCorrectUitgevoerd()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 2] = Colour.Black;
            game.Board[5, 5] = Colour.White;
            game.Board[6, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 2 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 1 0 0
            // 6   0 0 0 0 0 0 1 0
            // 7   0 0 0 0 0 0 0 2 <
            // Act
            game.Turn = Colour.Black;
            game.DoMove(7, 7);
            // Assert
            Assert.AreEqual(Colour.Black, game.Board[2, 2]);
            Assert.AreEqual(Colour.Black, game.Board[3, 3]);
            Assert.AreEqual(Colour.Black, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[5, 5]);
            Assert.AreEqual(Colour.Black, game.Board[6, 6]);
            Assert.AreEqual(Colour.Black, game.Board[7, 7]);
        }

        [Test]
        public void DoeZet_ZetAanDeRandRechtsOnder_Exception()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 2] = Colour.Black;
            game.Board[5, 5] = Colour.White;
            game.Board[6, 6] = Colour.White;
            //     0 1 2 3 4 5 6 7
            //                   v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 2 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 1 0 0
            // 6   0 0 0 0 0 0 1 0
            // 7   0 0 0 0 0 0 0 1 <
            // Act
            game.Turn = Colour.White;
            Exception ex = Assert.Throws<MoveNotPossibleException>(delegate { game.DoMove(7, 7); });
            Assert.That(ex.Message, Is.EqualTo("Zet (7,7) is niet mogelijk!"));

            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.Black, game.Board[2, 2]);
            Assert.AreEqual(Colour.White, game.Board[5, 5]);
            Assert.AreEqual(Colour.White, game.Board[6, 6]);

            Assert.AreEqual(Colour.None, game.Board[7, 7]);
        }

        [Test]
        public void DoeZet_ZetAanDeRandLinksBoven_ZetCorrectUitgevoerd()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 1] = Colour.White;
            game.Board[2, 2] = Colour.White;
            game.Board[5, 5] = Colour.Black;
            //     0 1 2 3 4 5 6 7
            //     v
            // 0   2 0 0 0 0 0 0 0  <
            // 1   0 1 0 0 0 0 0 0
            // 2   0 0 1 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 2 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0 
            // Act
            game.Turn = Colour.Black;
            game.DoMove(0, 0);
            // Assert
            Assert.AreEqual(Colour.Black, game.Board[0, 0]);
            Assert.AreEqual(Colour.Black, game.Board[1, 1]);
            Assert.AreEqual(Colour.Black, game.Board[2, 2]);
            Assert.AreEqual(Colour.Black, game.Board[3, 3]);
            Assert.AreEqual(Colour.Black, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[5, 5]);
        }

        [Test]
        public void DoeZet_ZetAanDeRandLinksBoven_Exception()
        {
            // Arrange
            Game game = new Game();
            game.Board[1, 1] = Colour.White;
            game.Board[2, 2] = Colour.White;
            game.Board[5, 5] = Colour.Black;
            //     0 1 2 3 4 5 6 7
            //     v
            // 0   1 0 0 0 0 0 0 0  <
            // 1   0 1 0 0 0 0 0 0
            // 2   0 0 1 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 2 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0          
            // Act
            game.Turn = Colour.White;
            //game.DoMove(0, 0);
            Exception ex = Assert.Throws<MoveNotPossibleException>(delegate { game.DoMove(0, 0); });
            Assert.That(ex.Message, Is.EqualTo("Zet (0,0) is niet mogelijk!"));


            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.White, game.Board[1, 1]);
            Assert.AreEqual(Colour.White, game.Board[2, 2]);

            Assert.AreEqual(Colour.Black, game.Board[5, 5]);

            Assert.AreEqual(Colour.None, game.Board[0, 0]);
        }

        [Test]
        public void DoeZet_ZetAanDeRandLinksOnder_ZetCorrectUitgevoerd()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 5] = Colour.White;
            game.Board[5, 2] = Colour.Black;
            game.Board[6, 1] = Colour.Black;
            //     0 1 2 3 4 5 6 7
            //     v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 1 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 2 0 0 0 0 0
            // 6   0 2 0 0 0 0 0 0
            // 7   1 0 0 0 0 0 0 0 <
            // Act
            game.Turn = Colour.White;
            game.DoMove(7, 0);
            // Assert
            Assert.AreEqual(Colour.White, game.Board[7, 0]);
            Assert.AreEqual(Colour.White, game.Board[6, 1]);
            Assert.AreEqual(Colour.White, game.Board[5, 2]);
            Assert.AreEqual(Colour.White, game.Board[4, 3]);
            Assert.AreEqual(Colour.White, game.Board[3, 4]);
            Assert.AreEqual(Colour.White, game.Board[2, 5]);
        }

        [Test]
        public void DoeZet_ZetAanDeRandLinksOnder_Exception()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 5] = Colour.White;
            game.Board[5, 2] = Colour.Black;
            game.Board[6, 1] = Colour.Black;
            //     0 1 2 3 4 5 6 7
            //     v
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 1 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 2 0 0 0 0 0
            // 6   0 2 0 0 0 0 0 0
            // 7   2 0 0 0 0 0 0 0 <
            // Act
            game.Turn = Colour.Black;
            Exception ex = Assert.Throws<MoveNotPossibleException>(delegate { game.DoMove(7, 0); });
            Assert.That(ex.Message, Is.EqualTo("Zet (7,0) is niet mogelijk!"));

            // Assert
            Assert.AreEqual(Colour.White, game.Board[3, 3]);
            Assert.AreEqual(Colour.White, game.Board[4, 4]);
            Assert.AreEqual(Colour.Black, game.Board[3, 4]);
            Assert.AreEqual(Colour.Black, game.Board[4, 3]);

            Assert.AreEqual(Colour.White, game.Board[2, 5]);
            Assert.AreEqual(Colour.Black, game.Board[5, 2]);
            Assert.AreEqual(Colour.Black, game.Board[6, 1]);

            Assert.AreEqual(Colour.None, game.Board[7, 7]);

            Assert.AreEqual(Colour.None, game.Board[7, 0]);
        }

        [Test]
        public void Pas_ZwartAanZetGeenZetMogelijk_ReturnTrueEnWisselBeurt()
        {
            // Arrange  (zowel wit als zwart kunnen niet meer)
            Game game = new Game();
            game.Board[0, 0] = Colour.White;
            game.Board[0, 1] = Colour.White;
            game.Board[0, 2] = Colour.White;
            game.Board[0, 3] = Colour.White;
            game.Board[0, 4] = Colour.White;
            game.Board[0, 5] = Colour.White;
            game.Board[0, 6] = Colour.White;
            game.Board[0, 7] = Colour.White;
            game.Board[1, 0] = Colour.White;
            game.Board[1, 1] = Colour.White;
            game.Board[1, 2] = Colour.White;
            game.Board[1, 3] = Colour.White;
            game.Board[1, 4] = Colour.White;
            game.Board[1, 5] = Colour.White;
            game.Board[1, 6] = Colour.White;
            game.Board[1, 7] = Colour.White;
            game.Board[2, 0] = Colour.White;
            game.Board[2, 1] = Colour.White;
            game.Board[2, 2] = Colour.White;
            game.Board[2, 3] = Colour.White;
            game.Board[2, 4] = Colour.White;
            game.Board[2, 5] = Colour.White;
            game.Board[2, 6] = Colour.White;
            game.Board[2, 7] = Colour.White;
            game.Board[3, 0] = Colour.White;
            game.Board[3, 1] = Colour.White;
            game.Board[3, 2] = Colour.White;
            game.Board[3, 3] = Colour.White;
            game.Board[3, 4] = Colour.White;
            game.Board[3, 5] = Colour.White;
            game.Board[3, 6] = Colour.White;
            game.Board[3, 7] = Colour.None;
            game.Board[4, 0] = Colour.White;
            game.Board[4, 1] = Colour.White;
            game.Board[4, 2] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[4, 4] = Colour.White;
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.None;
            game.Board[4, 7] = Colour.None;
            game.Board[5, 0] = Colour.White;
            game.Board[5, 1] = Colour.White;
            game.Board[5, 2] = Colour.White;
            game.Board[5, 3] = Colour.White;
            game.Board[5, 4] = Colour.White;
            game.Board[5, 5] = Colour.White;
            game.Board[5, 6] = Colour.None;
            game.Board[5, 7] = Colour.Black;
            game.Board[6, 0] = Colour.White;
            game.Board[6, 1] = Colour.White;
            game.Board[6, 2] = Colour.White;
            game.Board[6, 3] = Colour.White;
            game.Board[6, 4] = Colour.White;
            game.Board[6, 5] = Colour.White;
            game.Board[6, 6] = Colour.White;
            game.Board[6, 7] = Colour.None;
            game.Board[7, 0] = Colour.White;
            game.Board[7, 1] = Colour.White;
            game.Board[7, 2] = Colour.White;
            game.Board[7, 3] = Colour.White;
            game.Board[7, 4] = Colour.White;
            game.Board[7, 5] = Colour.White;
            game.Board[7, 6] = Colour.White;
            game.Board[7, 7] = Colour.White;

            //     0 1 2 3 4 5 6 7
            //     v
            // 0   1 1 1 1 1 1 1 1  
            // 1   1 1 1 1 1 1 1 1
            // 2   1 1 1 1 1 1 1 1
            // 3   1 1 1 1 1 1 1 0
            // 4   1 1 1 1 1 1 0 0
            // 5   1 1 1 1 1 1 0 2
            // 6   1 1 1 1 1 1 1 0
            // 7   1 1 1 1 1 1 1 1
            // Act
            game.Turn = Colour.Black;
            game.Pass();
            // Assert
            Assert.AreEqual(Colour.White, game.Turn);
        }

        [Test]
        public void Pas_WitAanZetGeenZetMogelijk_ReturnTrueEnWisselBeurt()
        {
            // Arrange  (zowel wit als zwart kunnen niet meer)
            Game game = new Game();
            game.Board[0, 0] = Colour.White;
            game.Board[0, 1] = Colour.White;
            game.Board[0, 2] = Colour.White;
            game.Board[0, 3] = Colour.White;
            game.Board[0, 4] = Colour.White;
            game.Board[0, 5] = Colour.White;
            game.Board[0, 6] = Colour.White;
            game.Board[0, 7] = Colour.White;
            game.Board[1, 0] = Colour.White;
            game.Board[1, 1] = Colour.White;
            game.Board[1, 2] = Colour.White;
            game.Board[1, 3] = Colour.White;
            game.Board[1, 4] = Colour.White;
            game.Board[1, 5] = Colour.White;
            game.Board[1, 6] = Colour.White;
            game.Board[1, 7] = Colour.White;
            game.Board[2, 0] = Colour.White;
            game.Board[2, 1] = Colour.White;
            game.Board[2, 2] = Colour.White;
            game.Board[2, 3] = Colour.White;
            game.Board[2, 4] = Colour.White;
            game.Board[2, 5] = Colour.White;
            game.Board[2, 6] = Colour.White;
            game.Board[2, 7] = Colour.White;
            game.Board[3, 0] = Colour.White;
            game.Board[3, 1] = Colour.White;
            game.Board[3, 2] = Colour.White;
            game.Board[3, 3] = Colour.White;
            game.Board[3, 4] = Colour.White;
            game.Board[3, 5] = Colour.White;
            game.Board[3, 6] = Colour.White;
            game.Board[3, 7] = Colour.None;
            game.Board[4, 0] = Colour.White;
            game.Board[4, 1] = Colour.White;
            game.Board[4, 2] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[4, 4] = Colour.White;
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.None;
            game.Board[4, 7] = Colour.None;
            game.Board[5, 0] = Colour.White;
            game.Board[5, 1] = Colour.White;
            game.Board[5, 2] = Colour.White;
            game.Board[5, 3] = Colour.White;
            game.Board[5, 4] = Colour.White;
            game.Board[5, 5] = Colour.White;
            game.Board[5, 6] = Colour.None;
            game.Board[5, 7] = Colour.Black;
            game.Board[6, 0] = Colour.White;
            game.Board[6, 1] = Colour.White;
            game.Board[6, 2] = Colour.White;
            game.Board[6, 3] = Colour.White;
            game.Board[6, 4] = Colour.White;
            game.Board[6, 5] = Colour.White;
            game.Board[6, 6] = Colour.White;
            game.Board[6, 7] = Colour.None;
            game.Board[7, 0] = Colour.White;
            game.Board[7, 1] = Colour.White;
            game.Board[7, 2] = Colour.White;
            game.Board[7, 3] = Colour.White;
            game.Board[7, 4] = Colour.White;
            game.Board[7, 5] = Colour.White;
            game.Board[7, 6] = Colour.White;
            game.Board[7, 7] = Colour.White;

            //     0 1 2 3 4 5 6 7
            //     v
            // 0   1 1 1 1 1 1 1 1  
            // 1   1 1 1 1 1 1 1 1
            // 2   1 1 1 1 1 1 1 1
            // 3   1 1 1 1 1 1 1 0
            // 4   1 1 1 1 1 1 0 0
            // 5   1 1 1 1 1 1 0 2
            // 6   1 1 1 1 1 1 1 0
            // 7   1 1 1 1 1 1 1 1
            // Act
            game.Turn = Colour.White;
            game.Pass();
            // Assert
            Assert.AreEqual(Colour.Black, game.Turn);
        }

        [Test]
        public void Afgelopen_ZetMogelijk_ReturnFalse()
        {
            // Arrange  (zowel wit als zwart kunnen niet meer ondanks dat er nog open spots zijn)
            Game game = new Game();
            game.Board[0, 0] = Colour.White;
            game.Board[0, 1] = Colour.White;
            game.Board[0, 2] = Colour.White;
            game.Board[0, 3] = Colour.White;
            game.Board[0, 4] = Colour.White;
            game.Board[0, 5] = Colour.White;
            game.Board[0, 6] = Colour.White;
            game.Board[0, 7] = Colour.White;
            game.Board[1, 0] = Colour.White;
            game.Board[1, 1] = Colour.White;
            game.Board[1, 2] = Colour.White;
            game.Board[1, 3] = Colour.White;
            game.Board[1, 4] = Colour.White;
            game.Board[1, 5] = Colour.White;
            game.Board[1, 6] = Colour.White;
            game.Board[1, 7] = Colour.White;
            game.Board[2, 0] = Colour.White;
            game.Board[2, 1] = Colour.White;
            game.Board[2, 2] = Colour.White;
            game.Board[2, 3] = Colour.White;
            game.Board[2, 4] = Colour.White;
            game.Board[2, 5] = Colour.White;
            game.Board[2, 6] = Colour.White;
            game.Board[2, 7] = Colour.White;
            game.Board[3, 0] = Colour.White;
            game.Board[3, 1] = Colour.White;
            game.Board[3, 2] = Colour.White;
            game.Board[3, 3] = Colour.White;
            game.Board[3, 4] = Colour.White;
            game.Board[3, 5] = Colour.White;
            game.Board[3, 6] = Colour.White;
            game.Board[3, 7] = Colour.None;
            game.Board[4, 0] = Colour.White;
            game.Board[4, 1] = Colour.White;
            game.Board[4, 2] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[4, 4] = Colour.White;
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.None;
            game.Board[4, 7] = Colour.None;
            game.Board[5, 0] = Colour.White;
            game.Board[5, 1] = Colour.White;
            game.Board[5, 2] = Colour.White;
            game.Board[5, 3] = Colour.White;
            game.Board[5, 4] = Colour.White;
            game.Board[5, 5] = Colour.White;
            game.Board[5, 6] = Colour.None;
            game.Board[5, 7] = Colour.Black;
            game.Board[6, 0] = Colour.White;
            game.Board[6, 1] = Colour.White;
            game.Board[6, 2] = Colour.White;
            game.Board[6, 3] = Colour.White;
            game.Board[6, 4] = Colour.White;
            game.Board[6, 5] = Colour.White;
            game.Board[6, 6] = Colour.White;
            game.Board[6, 7] = Colour.None;
            game.Board[7, 0] = Colour.White;
            game.Board[7, 1] = Colour.White;
            game.Board[7, 2] = Colour.White;
            game.Board[7, 3] = Colour.White;
            game.Board[7, 4] = Colour.White;
            game.Board[7, 5] = Colour.White;
            game.Board[7, 6] = Colour.White;
            game.Board[7, 7] = Colour.White;

            //     0 1 2 3 4 5 6 7
            //     v
            // 0   1 1 1 1 1 1 1 1  
            // 1   1 1 1 1 1 1 1 1
            // 2   1 1 1 1 1 1 1 1
            // 3   1 1 1 1 1 1 1 0
            // 4   1 1 1 1 1 1 0 0
            // 5   1 1 1 1 1 1 0 2
            // 6   1 1 1 1 1 1 1 0
            // 7   1 1 1 1 1 1 1 1
            // Act
            game.Turn = Colour.White;
            var actual = game.AnyMovesPossible();
            // Assert
            Assert.IsFalse(actual);
        }

        [Test]
        public void Afgelopen_ZetMogelijkAllesBezet_ReturnFalse()
        {
            // Arrange  (zowel wit als zwart kunnen niet meer omdat t hele bord VOL zit)
            Game game = new Game();
            game.Board[0, 0] = Colour.White;
            game.Board[0, 1] = Colour.White;
            game.Board[0, 2] = Colour.White;
            game.Board[0, 3] = Colour.White;
            game.Board[0, 4] = Colour.White;
            game.Board[0, 5] = Colour.White;
            game.Board[0, 6] = Colour.White;
            game.Board[0, 7] = Colour.White;
            game.Board[1, 0] = Colour.White;
            game.Board[1, 1] = Colour.White;
            game.Board[1, 2] = Colour.White;
            game.Board[1, 3] = Colour.White;
            game.Board[1, 4] = Colour.White;
            game.Board[1, 5] = Colour.White;
            game.Board[1, 6] = Colour.White;
            game.Board[1, 7] = Colour.White;
            game.Board[2, 0] = Colour.White;
            game.Board[2, 1] = Colour.White;
            game.Board[2, 2] = Colour.White;
            game.Board[2, 3] = Colour.White;
            game.Board[2, 4] = Colour.White;
            game.Board[2, 5] = Colour.White;
            game.Board[2, 6] = Colour.White;
            game.Board[2, 7] = Colour.White;
            game.Board[3, 0] = Colour.White;
            game.Board[3, 1] = Colour.White;
            game.Board[3, 2] = Colour.White;
            game.Board[3, 3] = Colour.White;
            game.Board[3, 4] = Colour.White;
            game.Board[3, 5] = Colour.White;
            game.Board[3, 6] = Colour.White;
            game.Board[3, 7] = Colour.White;
            game.Board[4, 0] = Colour.White;
            game.Board[4, 1] = Colour.White;
            game.Board[4, 2] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[4, 4] = Colour.White;
            game.Board[4, 5] = Colour.White;
            game.Board[4, 6] = Colour.Black;
            game.Board[4, 7] = Colour.Black;
            game.Board[5, 0] = Colour.White;
            game.Board[5, 1] = Colour.White;
            game.Board[5, 2] = Colour.White;
            game.Board[5, 3] = Colour.White;
            game.Board[5, 4] = Colour.White;
            game.Board[5, 5] = Colour.White;
            game.Board[5, 6] = Colour.Black;
            game.Board[5, 7] = Colour.Black;
            game.Board[6, 0] = Colour.White;
            game.Board[6, 1] = Colour.White;
            game.Board[6, 2] = Colour.White;
            game.Board[6, 3] = Colour.White;
            game.Board[6, 4] = Colour.White;
            game.Board[6, 5] = Colour.White;
            game.Board[6, 6] = Colour.White;
            game.Board[6, 7] = Colour.Black;
            game.Board[7, 0] = Colour.White;
            game.Board[7, 1] = Colour.White;
            game.Board[7, 2] = Colour.White;
            game.Board[7, 3] = Colour.White;
            game.Board[7, 4] = Colour.White;
            game.Board[7, 5] = Colour.White;
            game.Board[7, 6] = Colour.White;
            game.Board[7, 7] = Colour.White;

            //     0 1 2 3 4 5 6 7
            //     v
            // 0   1 1 1 1 1 1 1 1  
            // 1   1 1 1 1 1 1 1 1
            // 2   1 1 1 1 1 1 1 1
            // 3   1 1 1 1 1 1 1 2
            // 4   1 1 1 1 1 1 2 2
            // 5   1 1 1 1 1 1 2 2
            // 6   1 1 1 1 1 1 1 2
            // 7   1 1 1 1 1 1 1 1
            // Act
            game.Turn = Colour.White;
            var actual = game.AnyMovesPossible();
            // Assert
            Assert.IsFalse(actual);
        }

        [Test]
        public void Afgelopen_WelZetMogelijk_ReturnFalse()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //                     
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            //                     
            // Act
            game.Turn = Colour.White;
            var actual = game.IsFinished;
            // Assert
            Assert.IsFalse(actual);
        }



        [Test]
        public void OverwegendeKleur_Gelijk_ReturnKleurGeen()
        {
            // Arrange
            Game game = new Game();
            //     0 1 2 3 4 5 6 7
            //                     
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 0 0 0 0 0
            // 3   0 0 0 1 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            //                     
            // Act
            var actual = game.DominantColour();
            // Assert
            Assert.AreEqual(Colour.None, actual);
        }

        [Test]
        public void OverwegendeKleur_Zwart_ReturnKleurZwart()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 3] = Colour.Black;
            game.Board[3, 3] = Colour.Black;
            game.Board[4, 3] = Colour.Black;
            game.Board[3, 4] = Colour.Black;
            game.Board[4, 4] = Colour.White;

            //     0 1 2 3 4 5 6 7
            //                     
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 2 0 0 0 0
            // 3   0 0 0 2 2 0 0 0
            // 4   0 0 0 2 1 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            //                     
            // Act
            var actual = game.DominantColour();
            // Assert
            Assert.AreEqual(Colour.Black, actual);
        }

        [Test]
        public void OverwegendeKleur_Wit_ReturnKleurWit()
        {
            // Arrange
            Game game = new Game();
            game.Board[2, 3] = Colour.White;
            game.Board[3, 3] = Colour.White;
            game.Board[4, 3] = Colour.White;
            game.Board[3, 4] = Colour.White;
            game.Board[4, 4] = Colour.Black;


            //     0 1 2 3 4 5 6 7
            //                     
            // 0   0 0 0 0 0 0 0 0  
            // 1   0 0 0 0 0 0 0 0
            // 2   0 0 0 1 0 0 0 0
            // 3   0 0 0 1 1 0 0 0
            // 4   0 0 0 1 2 0 0 0
            // 5   0 0 0 0 0 0 0 0
            // 6   0 0 0 0 0 0 0 0
            // 7   0 0 0 0 0 0 0 0
            //                     
            // Act
            var actual = game.DominantColour();
            // Assert
            Assert.AreEqual(Colour.White, actual);
        }
    }
}

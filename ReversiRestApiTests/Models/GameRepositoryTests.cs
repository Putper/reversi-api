﻿using NUnit.Framework;
using ReversiRestApi.DAL;
using ReversiRestApi.Models;
using System.Linq;

namespace ReversiRestApiTests.Models
{
    [TestFixture]
    class GameRepositoryTests
    {
        TestGameRepository repository;

        [SetUp]
        public void SetUp()
        {
            repository = new TestGameRepository();
        }

        [Test]
        public void AddGame_AddedToGames()
        {
            var game = SerialisableGame.FromGame(new Game());
            repository.AddGame(game);
            Assert.IsTrue(repository.GetGames().Contains(game));
        }

        [Test]
        public void GetGames_ReturnsGames()
        {
            Assert.AreEqual(repository.Games, repository.GetGames());
        }

        [Test]
        public void GetGame_Exists_ReturnsGame()
        {
            string token = "TestToken";

            var game = SerialisableGame.FromGame(new Game());
            game.Token = token;
            repository.AddGame(game);

            var foundGame = repository.GetGame(token);
            Assert.AreEqual(game, foundGame);
        }

        [Test]
        public void GetGame_DoesntExist_ReturnsNull()
        {
            var foundGame = repository.GetGame("NonExistingTestToken");
            Assert.IsNull(foundGame);
        }
    }
}

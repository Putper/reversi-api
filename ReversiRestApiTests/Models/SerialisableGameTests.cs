﻿using NUnit.Framework;
using ReversiRestApi.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace ReversiRestApiTests.Models
{
    [TestFixture]
    class SerialisableGameTests
    {
        [Test]
        public void FromGame_HasSameValues()
        {

            Game game = new Game()
            {
                Turn = Colour.White,
                Description = "Description",
                Player1Token = "Player1Token",
                Player2Token = "Player2Token",
                Token = "GameToken",
                Board = null,
            };

            SerialisableGame game2 = SerialisableGame.FromGame(game);
            //Assert.AreEqual(game.Id, game2.Id);
            Assert.AreEqual(game.Turn, game2.Turn);
            Assert.AreEqual(game.Description, game2.Description);
            Assert.AreEqual(game.Player1Token, game2.Player1Token);
            Assert.AreEqual(game.Player2Token, game2.Player2Token);
            Assert.AreEqual(game.Token, game2.Token);
            Assert.AreEqual(game.Board, game2.Board);
        }

        [Test]
        public void ToGame_HasSameValues()
        {
            var sGame = new SerialisableGame()
            {
                Turn = Colour.White,
                Description = "Description",
                Player1Token = "Player1Token",
                Player2Token = "Player2Token",
                Token = "GameToken",
                Board = null
            };
            var game = sGame.ToGame();

            Assert.AreEqual(sGame.Turn, game.Turn);
            Assert.AreEqual(sGame.Description, game.Description);
            Assert.AreEqual(sGame.Player1Token, game.Player1Token);
            Assert.AreEqual(sGame.Player2Token, game.Player2Token);
            Assert.AreEqual(sGame.Token, game.Token);
            Assert.AreEqual(sGame.Board, game.Board);
        }

        [Test]
        public void BoardFromString()
        {
            string stringBoard = "10322";
            var sGame = new SerialisableGame()
            {
                Turn = Colour.White,
                Description = "Description",
                Player1Token = "Player1Token",
                Player2Token = "Player2Token",
                Token = "GameToken",
                Board = stringBoard
            };
            var game = sGame.ToGame();

            Colour[,] expectedBoard = new Colour[,]
            {
                { Colour.White, Colour.None },
                { Colour.Black, Colour.Black }
            };
            Assert.AreEqual(expectedBoard, game.Board);
        }

        [Test]
        public void StringToBoard()
        {
            Colour[,] board = new Colour[2, 2]
            {
                { Colour.White, Colour.None },
                { Colour.Black, Colour.Black }
            };

            var game = new Game() { Board = board };
            var sGame = SerialisableGame.FromGame(game);
            Assert.AreEqual("10322", sGame.Board);
        }

        //[Test]
        //public void BoardToJaggedArray()
        //{
        //    Colour[,] board = new Colour[4, 4];
        //    board[1, 1] = Colour.Black;
        //    board[1, 2] = Colour.White;
        //    board[1, 3] = Colour.None;
        //    string[][] newBoard = SerialisableGame.BoardToJaggedArray(board);
        //    Assert.AreEqual("Black", newBoard[1][1].ToString());
        //    Assert.AreEqual("White", newBoard[1][2].ToString());
        //    Assert.AreEqual("None", newBoard[1][3].ToString());
        //}
    }
}
